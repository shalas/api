from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
db = SQLAlchemy(app)


class Company(db.Model):
    company_id = db.Column(db.Integer, primary_key=True)
    company_name = db.Column(db.String(100), unique=True, nullable=False)
    company_workers = db.Column(db.String(100), nullable=True)
    company_worker_count = db.Column(db.Integer, nullable=True)

    def __repr__(self):
        return f"{self.company_id} - " \
               f"{self.company_name}" \
               f"{self.company_worker_count}" \
               f"{self.company_workers}"


class Worker(db.Model):
    worker_id = db.Column(db.Integer, primary_key=True)
    worker_name = db.Column(db.String(100), unique=True, nullable=False)
    worker_company_name = db.Column(db.String(100))
    job_name = db.Column(db.String(100))
    worker_level = db.Column(db.String(20))
    worker_price = db.Column(db.Integer)

    def __repr__(self):
        return f'{self.worker_id} -' \
               f'{self.worker_name} -' \
               f'{self.worker_company_name} -' \
               f'{self.job_name} -' \
               f'{self.worker_level} -' \
               f'{self.worker_price}'


class Job(db.Model):
    job_id = db.Column(db.Integer, primary_key=True)
    job_name = db.Column(db.String(100), nullable=False)
    job_price = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f'{self.job_name} -' \
               f'{self.job_price} -' \
               f'{self.job_id}'


@app.route('/jobs')
def get_jobs():
    jobs = Job.query.all()
    output = []

    for job in jobs:
        job_data = {'job_id': job.job_id,
                    'job_name': job.job_name,
                    'job_price': job.job_price}

        output.append(job_data)
    return {'Jobs': output}


@app.route('/job/<id>')
def get_job(id):
    job = Job.query.get_or_404(id)
    return {'job_id': job.job_id,
            'job_name': job.job_name,
            'job_price': job.job_price}


@app.route('/job', methods=['POST'])
def create_job():
    job = Job(job_name=request.json['job_name'],
              job_price=request.json['job_price'])
    db.session.add(job)
    db.session.commit()
    return {'job_id': job.job_id,
            'job_name': job.job_name,
            'job_price': job.job_price}


@app.route('/job/<id>', methods=['PUT'])
def put_job(id):
    job = Job.query.get(id)
    job.job_name = request.json['job_name']
    job.job_price = request.json['job_price']
    db.session.commit()
    return {'job_name': job.job_name}


@app.route('/job/<id>', methods=['DELETE'])
def delete_job(id):
    job = Job.query.get(id)
    if job is None:
        return Company.query.get_or_404(id)
    db.session.delete(job)
    db.session.commit()
    return {"Message": f"The job with id{id} was deleted"}


@app.route('/workers')
def get_workers():
    workers = Worker.query.all()
    output = []

    for worker in workers:
        worker_data = {'worker_id': worker.worker_id,
                       'worker_name': worker.worker_name,
                       'worker_company_name': worker.worker_company_name,
                       'job_name': worker.job_name,
                       'worker_level': worker.worker_level,
                       'worker_price': worker.worker_price}

        output.append(worker_data)
    return {"Workers": output}


@app.route('/worker/<id>')
def get_worker(id):
    worker = Worker.query.get_or_404(id)
    return {'worker_id': worker.worker_id,
                       'worker_name': worker.worker_name,
                       'worker_company_name': worker.worker_company_name,
                       'job_name': worker.job_name,
                       'worker_level': worker.worker_level,
                       'worker_price': worker.worker_price}


@app.route('/worker', methods=['POST'])
def create_worker():
    companies = Company.query.all()
    user_request_company_name = request.json['worker_company_name']
    requested_company_state = False
    for company in companies:
        if user_request_company_name in company.company_name:
            requested_company_state = True

    jobs = Job.query.all()
    user_request_job_name = request.json['job_name']
    requested_job_state = False
    for job in jobs:
        if user_request_job_name in job.job_name:
            requested_job_state = True

    if requested_company_state and requested_job_state:
        worker = Worker(worker_name=request.json['worker_name'],
                        worker_company_name=request.json['worker_company_name'],
                        job_name=request.json['job_name'],
                        worker_level=request.json['worker_level'],
                        worker_price=request.json['worker_price'])
        db.session.add(worker)
        db.session.commit()
        return {'worker_name': worker.worker_name,
                'worker_level': worker.worker_level}
    else:
        return Company.query.get_or_404(request.json['worker_company_name'])


@app.route('/worker/<id>', methods=['PUT'])
def put_worker(id):
    worker = Worker.query.get(id)
    worker.worker_company_name = request.json['worker_company_name']
    worker.worker_level = request.json['worker_level']
    worker.worker_price = request.json['worker_price']
    db.session.commit()
    return {'worker_name': worker.worker_name,
            'worker_company_name': worker.worker_company_name,
            'worker_level': worker.worker_level,
            'worker_price': worker.worker_price}


@app.route('/worker/<id>', methods=['DELETE'])
def delete_worker(id):
    worker = Worker.query.get(id)
    if worker is None:
        return Worker.query.get_or_404(id)
    db.session.delete(worker)
    db.session.commit()
    return {"Message": f"Worker with id: {id} was deleted"}


@app.route('/companies')
def get_companies():
    companies = Company.query.all()
    workers = Worker.query.all()

    workers_company_name = []
    for worker in workers:
        workers_company_name.append(worker.worker_company_name)

    output = []

    for company in companies:
        company_data = {'company_id': company.company_id,
                        'company_name': company.company_name,
                        'company_workers': company.company_workers,
                        'company_workers_count': workers_company_name.count(company.company_name)}

        output.append(company_data)
    return {"Companies": output}


@app.route('/company/<id>')
def get_company(id):

    workers = Worker.query.all()

    workers_company_name = []
    for worker in workers:
        workers_company_name.append(worker.worker_company_name)

    company = Company.query.get_or_404(id)
    return {'company_id': company.company_id,
            'company_name': company.company_name,
            'company_workers': company.company_workers,
            'company_workers_count': workers_company_name.count(company.company_name)}


@app.route('/company', methods=['POST'])
def create_company():
    company = Company(company_name=request.json['company_name'])
    db.session.add(company)
    db.session.commit()
    return {'company_id': company.company_id,
            'company_name': company.company_name}


@app.route('/company/<id>', methods=['DELETE'])
def delete_company(id):
    company = Company.query.get(id)
    if company is None:
        return Company.query.get_or_404(id)
    db.session.delete(company)
    db.session.commit()
    return {"Message": f"The company with id{id} was deleted"}

